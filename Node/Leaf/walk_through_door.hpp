#ifndef WALK_THROUGH_DOOR_HPP
#define WALK_THROUGH_DOOR_HPP

#include <stdio.h>

#include "leaf.hpp"
#include "tree_status.hpp"
#include "door_status.hpp"

namespace behaviortree {
	namespace node {
		namespace leaf {
			class WalkThroughDoor : public Leaf {
			public:
				explicit WalkThroughDoor(const DoorStatus* doorStatus)
					: m_doorStatus(doorStatus)
				{};
				Status Execute() const {
					if (m_doorStatus->isOpen) {
						printf("Walking through the door!\n");
						return Status::kSuccess;
					}
					else {
						printf("Failed to walk through the door\n");
						return Status::kFailure;
					}
				}
			private:
				const DoorStatus* m_doorStatus;
			};
		}
	}
}

#endif // !WALK_THROUGH_DOOR_HPP