#ifndef NODE_HPP
#define NODE_HPP

#include "tree_status.hpp"

namespace behaviortree {
	namespace node {
		class Node {
		public:
			virtual Status Execute() const = 0;
		};
	}
}

#endif // !NODE_HPP