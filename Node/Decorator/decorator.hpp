#ifndef DECORATOR_HPP
#define DECORATOR_HPP

#include "node/node.hpp"
#include "tree_status.hpp"

namespace behaviortree {
	namespace node {
		namespace decorator {
			class Decorator : public Node {
			public:
				inline void SetChild(Node& input)
				{
					this->child = &input;
				}

			protected:
				Node* child;
			};
		}
	}
}

#endif // !DECORATOR_HPP