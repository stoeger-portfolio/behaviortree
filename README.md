Simple behavior tree implementation.

Simple tasks an AI can use for walking through a closed door.
The door states can be changed in the code.

AI simply outputs the action.