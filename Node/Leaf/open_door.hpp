#ifndef OPEN_DOOR_HPP
#define OPEN_DOOR_HPP

#include <stdio.h>

#include "leaf.hpp"
#include "tree_status.hpp"
#include "door_status.hpp"

namespace behaviortree {
	namespace node {
		namespace leaf {
			class OpenDoor : public Leaf {
			public:
				explicit OpenDoor(DoorStatus* doorStatus)
					: m_doorStatus(doorStatus)
				{};

				Status Execute() const {
					if (m_doorStatus->isOpen)
					{
						printf("The door is already open\n");
						return Status::kSuccess;
					}
					if ((m_doorStatus->distance == 1) && (!m_doorStatus->isLocked))
					{
						printf("Opening Door!\n");
						m_doorStatus->isOpen = true;
						return Status::kSuccess;
					}
					else {
						printf("Not possible to open the door!\n");
						return Status::kFailure;
					}
				}
			private:
				DoorStatus* m_doorStatus;
			};
		}
	}
}

#endif // !OPEN_DOOR_HPP