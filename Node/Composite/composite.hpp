#ifndef COMPOSITE_HPP
#define COMPOSITE_HPP

#include <vector>

#include "node/node.hpp"
#include "tree_status.hpp"

namespace behaviortree {
	namespace node {
		namespace composite {
			class Composite : public Node {
			public:
				virtual Status Execute() const = 0;
				inline void AddChildren(Node& child)
				{
					m_children.push_back(&child);
				}

			protected:
				std::vector<Node*> m_children;

			};
		}
	}
}

#endif // !COMPOSITE_HPP
