#ifndef SEQUENCE_HPP
#define SEQUENCE_HPP

#include "composite.hpp"
#include "tree_status.hpp"

namespace behaviortree {
	namespace node {
		namespace composite {
			class Sequence : public Composite {
			public:
				Status Execute() const
				{
					for (Node* child : m_children)
					{
						if (child->Execute() == Status::kFailure)
						{
							return Status::kFailure;
						}
					}
					return Status::kSuccess;
				}
			};
		}
	}
}

#endif // !SEQUENCE_HPP
