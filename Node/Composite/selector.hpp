#ifndef SELECTOR_HPP
#define SELECTOR_HPP

#include "composite.hpp"
#include "tree_status.hpp"

namespace behaviortree {
	namespace node {
		namespace composite {
			class Selector : public Composite {
			public:
				Status Execute() const
				{
					for (Node* child : m_children)
					{
						if (child->Execute() == Status::kSuccess)
						{
							return Status::kSuccess;
						}
					}
					return Status::kFailure;
				}
			};
		}
	}
}

#endif // !SELECTOR_HPP
