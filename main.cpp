#include "node/composite/sequence.hpp"
#include "node/composite/selector.hpp"
#include "node/leaf/walk_to_door.hpp"
#include "node/leaf/open_door.hpp"
#include "node/leaf/unlock_door.hpp"
#include "node/leaf/smash_door.hpp"
#include "node/leaf/walk_through_door.hpp"
#include "node/leaf/close_door.hpp"
#include "door_status.hpp"
#include "tree_status.hpp"

int main(void)
{
	behaviortree::DoorStatus* status = new behaviortree::DoorStatus();

	behaviortree::node::composite::Sequence root;

	status->distance = 5;
	status->isOpen = true;
	status->isLocked = true;

	behaviortree::node::composite::Selector selector;
	behaviortree::node::composite::Sequence sequence;

	behaviortree::node::leaf::WalkToDoor walkToDoor(status);
	behaviortree::node::leaf::OpenDoor openDoor(status);
	behaviortree::node::leaf::UnlockDoor unlockDoor(status);
	behaviortree::node::leaf::SmashDoor smashDoor(status);
	behaviortree::node::leaf::WalkThroughDoor walkThroughDoor(status);
	behaviortree::node::leaf::CloseDoor closeDoor(status);

	sequence.AddChildren(unlockDoor);
	sequence.AddChildren(openDoor);

	selector.AddChildren(openDoor);
	selector.AddChildren(sequence);
	selector.AddChildren(smashDoor);

	root.AddChildren(walkToDoor);
	root.AddChildren(selector);
	root.AddChildren(walkThroughDoor);
	root.AddChildren(closeDoor);

	root.Execute();

	delete status;

	return 0;
}
