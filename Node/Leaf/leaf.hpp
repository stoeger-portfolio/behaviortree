#ifndef LEAF_HPP
#define LEAF_HPP

#include "node/node.hpp"

namespace behaviortree {
	namespace node {
		namespace leaf {
			class Leaf : public Node {
			};
		}
	}
}

#endif // !LEAF_HPP