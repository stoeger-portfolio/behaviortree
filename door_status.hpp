#ifndef DOOR_STATUS_HPP
#define DOOR_STATUS_HPP

namespace behaviortree
{
	struct DoorStatus
	{
		bool isOpen;
		bool isLocked;
		int distance;
	};
}
#endif // !DOOR_STATUS_HPP
