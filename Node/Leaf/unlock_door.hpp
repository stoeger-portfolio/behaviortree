#ifndef UNLOCK_DOOR_HPP
#define UNLOCK_DOOR_HPP

#include <stdio.h>

#include "leaf.hpp"
#include "tree_status.hpp"
#include "door_status.hpp"

namespace behaviortree {
	namespace node {
		namespace leaf {
			class UnlockDoor : public Leaf {
			public:
				explicit UnlockDoor(DoorStatus* doorStatus)
					: m_doorStatus(doorStatus)
				{};

				Status Execute() const {
					if ((m_doorStatus->distance == 1) && (!m_doorStatus->isOpen) && (m_doorStatus->isLocked))
					{
						printf("Unlock Door!\n");
						m_doorStatus->isLocked = false;
						return Status::kSuccess;
					}
					else {
						printf("Can not unlock the Door!\n");
						return Status::kFailure;
					}
					return Status::kFailure;
				}
			private:
				DoorStatus* m_doorStatus;
			};
		}
	}
}

#endif // !UNLOCK_DOOR_HPP
