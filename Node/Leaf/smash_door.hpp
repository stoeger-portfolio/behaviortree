#ifndef SMASH_DOOR_HPP
#define SMASH_DOOR_HPP

#include <stdio.h>

#include "leaf.hpp"
#include "tree_status.hpp"
#include "door_status.hpp"

namespace behaviortree {
	namespace node {
		namespace leaf {
			class SmashDoor : public Leaf {
			public:
				explicit SmashDoor(DoorStatus* doorStatus)
					: m_doorStatus(doorStatus)
				{};
				Status Execute() const {
					if (m_doorStatus->distance == 1)
					{
						printf("Smash Door!\n");
						m_doorStatus->isLocked = false;
						m_doorStatus->isOpen = true;
						return Status::kSuccess;
					}
					else {
						printf("Can not smash in the door!\n");
						return Status::kFailure;
					}
				}
			private:
				DoorStatus* m_doorStatus;
			};
		}
	}
}

#endif // !SMASH_DOOR_HPP