#ifndef CLOSE_DOOR_HPP
#define CLOSE_DOOR_HPP

#include <stdio.h>

#include "leaf.hpp"
#include "tree_status.hpp"
#include "door_status.hpp"

namespace behaviortree {
	namespace node {
		namespace leaf {
			class CloseDoor : public Leaf {
			public:
				explicit CloseDoor(DoorStatus* doorStatus)
					: m_doorStatus(doorStatus)
				{};
				Status Execute() const {
					if (m_doorStatus->isOpen) {
						printf("Closing the door!\n");
						m_doorStatus->isOpen = false;
						return Status::kSuccess;
					}
					else {
						printf("Can not close the door!");
						return Status::kFailure;
					}
				}
			private:
				DoorStatus* m_doorStatus;
			};
		}
	}
}

#endif // !CLOSE_DOOR_HPP