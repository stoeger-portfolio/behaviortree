#ifndef TREESTATUS_HPP
#define TREESTATUS_HPP

namespace behaviortree 
{
	enum Status {
		kSuccess = 0,
		kFailure = 1,
		kRunning = 2
	};
}
#endif // !TREESTATUS_HPP
