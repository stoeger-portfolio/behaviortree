#ifndef WALK_TO_DOOR_HPP
#define WALK_TO_DOOR_HPP

#include <stdio.h>

#include "leaf.hpp"
#include "tree_status.hpp"
#include "door_status.hpp"

namespace behaviortree {
	namespace node {
		namespace leaf {
			class WalkToDoor : public Leaf {
			public:
				explicit WalkToDoor(DoorStatus* doorStatus)
					: m_doorStatus(doorStatus)
				{};

				Status Execute() const {
					while (m_doorStatus->distance > 1) {
						--m_doorStatus->distance;
						printf("Walking to door! Current Distance: %d\n", m_doorStatus->distance);
					}
					return Status::kSuccess;
				}
			private:
				DoorStatus* m_doorStatus;
			};
		}
	}
}

#endif // !WALK_TO_DOOR_HPP