#ifndef INVERTER_HPP
#define INVERTER_HPP

#include "decorator.hpp"
#include "tree_status.hpp"

namespace behaviortree {
	namespace node {
		namespace decorator {
			class Inverter : public Decorator {
			public:
				Status Execute() const
				{
					Status state = child->Execute();
					if (state == kRunning) {
						return state;
					}
					return state == Status::kSuccess ? Status::kFailure : Status::kSuccess;
				}
			};
		}
	}
}

#endif // !INVERTER_HPP